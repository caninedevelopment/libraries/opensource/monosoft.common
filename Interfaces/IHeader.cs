﻿namespace Monosoft.Common.Interfaces
{
    public interface IHeader
    {
        string Token { get; set; }
        string User { get; set; }
    }
}
