﻿namespace Monosoft.Common.Interfaces
{
    public interface IClient
    {
        string InitiatedByIp { get; set; }
    }
}
