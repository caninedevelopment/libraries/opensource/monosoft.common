﻿namespace Monosoft.Common.Command.Interfaces
{
    public interface IDtoRequest
    {
        void Validate();
    }
}