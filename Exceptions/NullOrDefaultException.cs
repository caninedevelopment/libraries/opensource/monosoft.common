﻿namespace Monosoft.Common.Exceptions
{
    public class NullOrDefaultException : LocalizedException
    {
        /// <summary>
        /// propertyName should be filled out by using "nameof()"
        /// </summary>
        /// <param name="propertyName">please use nameof()</param>
        public NullOrDefaultException(string propertyName) : base("A value should be provided for the property " + propertyName)
        {
            Error = LocalizedString.NullOrDefault(propertyName);
        }

        public NullOrDefaultException(string message, string propertyName) : base(message)
        {
            Error = LocalizedString.NullOrDefault(propertyName);
        }
    }
}