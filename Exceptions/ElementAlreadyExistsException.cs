﻿namespace Monosoft.Common.Exceptions
{
    public class ElementAlreadyExistsException : LocalizedException
    {
        public ElementAlreadyExistsException(string message, string itemId) : base(message)
        {
            Error = LocalizedString.ItemAlreadyExists(itemId);
        }
    }
}
