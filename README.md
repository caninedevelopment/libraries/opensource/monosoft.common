## Introduction
These common libs implements the most common used classes within Monosoft projects.
The are avaiable as nuget packages at https://gitlab.com/api/v4/projects/21192147/packages/nuget/index.json 

# Reference links

- [GitLab packages](https://gitlab.com/api/v4/projects/21192147/packages/nuget/index.json)
- [Monosoft homepage](https://www.monosoft.dk/)

## Update nuget

Pleaes update the packages with the following commands:

nuget push ./bin/Debug/Monosoft.Common.X.X.X.nupkg -Source Monosoft 


