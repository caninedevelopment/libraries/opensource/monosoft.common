﻿using System;

namespace Monosoft.Common
{
    public partial class LocalizedString
    {
        public static LocalizedString OK
        {
            get
            {
                return LocalizedString.Create(new Guid("bfe4ba64-fcec-4af4-b38b-7f039f96354d"), "OK");
            }
        }

        public static LocalizedString UnknownOperation
        {
            get
            {
                return LocalizedString.Create(new Guid("ec07beab-8be4-46b4-8ebb-fdcf1b912673"), "Unknown operation");
            }
        }

        public static LocalizedString MissingCredentials
        {
            get
            {
                return LocalizedString.Create(new Guid("6d6c10f3-d7f3-463c-b572-c2684b9999eb"), "Missing credentials");
            }
        }

        public static LocalizedString Error
        {
            get
            {
                return LocalizedString.Create(new Guid("9c360715-f5fe-4fd0-ad13-270e5ffbccdb"), "Error");
            }
        }

        internal static LocalizedString ItemAlreadyExists(string itemId)
        {
            return LocalizedString.Create(new Guid("ae3f4b65-fa8c-4b4e-a612-b6328bdc8099"), $"The item '{itemId}' already exists");
        }

        internal static LocalizedString ItemDoesNotExist(string itemId)
        {
            return LocalizedString.Create(new Guid("4525ab8c-a95a-4ab6-9677-3d4b1532b42d"), $"The item '{itemId}' does not exist");
        }

        internal static LocalizedString NullOrDefault(string propertyName)
        {
            return LocalizedString.Create(new Guid("13effc5c-709d-4102-a854-c772e16ca294"), $"The property '{propertyName}' must be provided");
        }

        internal static LocalizedString ValidationError(string propertyName)
        {
            return LocalizedString.Create(new Guid("f19f64c3-bd30-48cd-93d4-706d139a3740"), $"The property '{propertyName}' is not valid");
        }
    }
}
